FROM rocker/r-ver:4.1.1

RUN apt-get -y update && apt-get install -y \
    libudunits2-dev \
    libgdal-dev \
    libgeos-dev \
    libproj-dev \
    libmysqlclient-dev 

RUN install2.r --error --skipinstalled \
data.table \
lubridate \
rgdal \
raster \
ncdf4 \
evd \
lmomco \
fitdistrplus \
logger

RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

COPY . .

CMD  R -e "source('exec_index.R')" 

