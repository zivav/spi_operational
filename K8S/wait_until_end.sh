#!/bin/bash

#============================================================================
#
#  This script waits for the end of the job, retrieves the exit code and prints the pod log
#
#  Credits:
#  https://stackoverflow.com/questions/55073453/wait-for-kubernetes-job-to-complete-on-either-failure-success-using-command-line
#
#============================================================================

SCRIPT_VERSION="0.3"

set +x 

_log() {
    level="$1"
    dest="$2"
    msg="${@:3}"
    ts=$(date -u +"%Y-%m-%d %H:%M:%S")

    if [ "$dest" = "stdout" ]; then
       echo "$ts : $msg"
    elif [ "$dest" = "stderr" ]; then
       >&2 echo "$ts : $msg"
    fi

    if [ -n "$log_path" ]; then
       echo "$ts $level $msg" >> "$log_path"
    fi
}

log_debug() {
    _log "DEBUG" "stdout" "$@"
}

log_info() {
    _log "INFO " "stdout" "$@"
}

log_warning() {
    _log "WARN " "stderr" "$@"
}

log_error() {
    _log "ERROR" "stderr" "$@"
}

if [ -z "$1" ]
  then
    log_info "No argument supplied"
    exit 1
fi

log_info "Starting job $1"
# wait for completion as background process - capture PID
kubectl -n default wait --for=condition=complete job/$1 --timeout=24h &
completion_pid=$!

# wait for failure as background process - capture PID
kubectl -n default wait --for=condition=failed job/$1  --timeout=24h && exit 1 &
failure_pid=$!

log_info "Waiting..."
# capture exit code of the first subprocess to exit
wait -n $completion_pid $failure_pid

# store exit code in variable
exit_code=$?
log_info "Exit code is $exit_code"


# get the log from the pod
pod=$(kubectl -n default get pods -l job-name=$1 --no-headers -o custom-columns=":metadata.name" --sort-by=.metadata.creationTimestamp | tac | head -1)

log_info "Get the log of the pod $pod"

kubectl -n default logs $pod 

DATE=$(date +%Y-%m-%d)

kubectl -n default logs $pod > log_$DATE.txt

if (( $exit_code == 0 )); then
  log_info "Job $1 completed"
else
  log_info "Job $1 failed with exit code ${exit_code}, exiting..."
fi

exit $exit_code
