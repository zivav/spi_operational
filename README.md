   
# Calculation of meteorological indices

## Introduction

Repo `spi_operational` contains all scripts for operational calculation of the following drought indices:
- relative precipitation anomalies (REL_RR): based on daily precipitation data (RR),
- Standardised Precipitation Index (SPI): based on daily precipitation data (RR), 
- Standardised Precipitation Evapotranspiration Index (SPEI): based on daily surface water balance (precipitation-potential evapotranspiration (RR-ETP)) data,
- Standardised Snowpack Index (SSPI): based on daily snow water equivalent data (SWE).

The scripts are organised in the following manner:  
```
exec_index.R      # Main pipeline script to execute the calculation of selected indices for selected aggregation periods

 + 01_functions               # Holds the functions that are used in this pipeline, adapted from SCI package (Gudmundsson & Stagge, 2016)
 |-- distrstart.R             # functions that produce parameter estimates for specific distributions (distr) that are useful as starting 
 |                            # values for maximum likelihood estimation
 |-- sci_distributions.R      # functions defining additional distributions (distr) for parameter estimation
 |-- sci_daily                # functions to identify parameters for the standardized climate index transformation and to apply the transformation (based on input data aggregations with a daily time-step)

 + 02_scripts		       # Holds the necessary scripts that are used in this pipeline
 |-- raster_func.R            # functions to manipulate raster data files (nc to df, df to tif) and calculate surface water balance from ETP and RR
 |-- index_calc_func.R        # functions to prepare data aggregations from input data and calculate indices using functions in directory 01_functions
```

## Input requirements

Script `exec_index.R` reads daily input data (RR, ETP and SWE) from specified mounted directories (individual directory for each variable). Input data files should be in `.nc` format, with daily data organized by year. The name of the file should include the year of data in the form *YYYY*.

Script reads in necessary `.nc` files for the calculation of indices between `START_DATE` and `END_DATE`. Indices are calculated for different aggregation periods in the following order: REL_RR(-1,-2,-3,-6,-12), SPI(-1,-2,-3,-6,-12), SPEI(-1,-2,-3,-6,-12) and SSPI(-10d,-30d).  

Default `END_DATE = Sys.Date()-5` (today-5) and `START_DATE = END_DATE` (i.e. indices are only calculated for one day). 

`START_DATE` and `END_DATE` can be changed by setting the environmental variables in the Dockerfile, for example:

```
-e START_DATE='2022-03-01' 
-e END_DATE='2022-07-01'
```
to calculate indices for days between '2022-03-01' and '2022-07-01'.

Errors are returned and execution stops if:
- required `.nc` files to calculate indices do not exist in specified directory,
- data for `END_DATE` is not available,
- input data is not consecutive, i.e. there are missing days of data between `START_DATE` and `END_DATE`. 

At least **365** consecutive days of RR and ETP data prior to `START_DATE` are needed to calculate SPI, REL_RR and SPEI and at least **30** consecutive days of SWE data prior to `START_DATE` are needed to calculate SSPI. RR and ETP data should be in units mm day<sup>-1</sup> and SWE data should be in units kg m<sup>-2</sup>.

Static files used in the calculations (such as masks, reference period parameters and empty rasters) are also read from a specified mounted directory.


## Output structure 

As output, script `exec_index.R` calculates indices and creates daily `.tif` files for days between `START_DATE` and `END_DATE` and writes them into a directory with the following structure:
```
	+ REL_RR-1
	+ REL_RR-2
	+ REL_RR-3
	+ REL_RR-6
	+ REL_RR-12
	+ SPI-1
	+ SPI-2
	+ SPI-3
	+ SPI-6
	+ SPI-12
	+ SPEI-1
	+ SPEI-2
	+ SPEI-3
	+ SPEI-6
	+ SPEI-12
	+ SSPI-10d
	+ SSPI-30d
```
Output `.tif` files are created and saved to specified directories regardless if the last days have already been calculated and created (they are overwritten). In case the directories do not exist, they are created first.

## Building and running

System can be built with standard docker command once the source code (including Dockerfile) is pulled from the repository:
```
docker build -t exec_index .
```

In order to run index calculation script, several folders should be mounted. 

```docker
docker run -e END_DATE='2022-07-01' -e START_DATE='2022-03-03' \
             -v /mnt/CEPH_PRODUCTS/ADO/meteo/02_downscaled/total_precipitation:/ZAMG/RR             \
			 -v /mnt/CEPH_PRODUCTS/ADO/meteo/02_downscaled/potential_evapotranspiration:/ZAMG/ETP   \
			 -v /mnt/CEPH_PRODUCTS/ADO/meteo/03_snow-grid:/ZAMG/SWE                        \
			 -v /mnt/CEPH_PRODUCTS/ADO/00_static_data/spi_operational/stats:/ARSO/STATS                                       \
			 -v /mnt/CEPH_PRODUCTS/ADO/:/ARSO/OUTPUT                                     \       
       exec_index:latest
```

It has been tested with these mount points on CEPH_PROJECTS
```
             -v /mnt/ADO/ZAMG/production/02_downscaled/total_precipitation:/ZAMG/RR             \
			 -v /mnt/ADO/ZAMG/production/02_downscaled/potential_evapotranspiration:/ZAMG/ETP   \
			 -v /mnt/ADO/ZAMG/production/03_snow-grid:/ZAMG/SWE                        \
			 -v /mnt/ADO/ARSO/stats:/ARSO/STATS                                       \
			 -v /mnt/ADO/ARSO/output:/ARSO/OUTPUT                                     \  
```

First three mounted folders have to contain input data files with daily RR, ETP and SWE data respectively (as explained in section Input requirements). In operational pipeline **folder containing operational data files should be mounted** to docker mount points `/ZAMG/RR` for precipitation, `/ZAMG/ETP` for potential evapotranspiration and `/ZAMG/SWE` for snow water equivalent.

Further, there are some static files prepared (reference data parameters, masks etc.) which are stored in `/mnt/ADO/ARSO/stats` and should be mounted to `/ARSO/STATS`. Finally, the script writes output files (as explained in section Output structure) to docker path `/ARSO/OUTPUT`, which is currently accessing files from `/mnt/ADO/ARSO/output`. 



